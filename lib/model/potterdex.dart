import 'package:meta/meta.dart';
import 'dart:convert';

List<Welcome> welcomeFromJson(String str) =>
    List<Welcome>.from(json.decode(str).map((x) => Welcome.fromJson(x)));

String welcomeToJson(List<Welcome> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Welcome {
  Welcome({
    required this.name,
    required this.gender,
    required this.house,
    required this.dateOfBirth,
    required this.patronus,
    required this.actor,
    required this.ancestry,
  });

  final String name;
  final String gender;
  final String house;
  final String dateOfBirth;
  final String patronus;
  final String actor;
  final String ancestry;

  factory Welcome.fromJson(Map<String, dynamic> json) => Welcome(
        name: json["name"],
        gender: json["gender"],
        house: json["house"],
        dateOfBirth: json["dateOfBirth"],
        patronus: json["patronus"],
        actor: json["actor"],
        ancestry: json["ancestry"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "gender": gender,
        "house": house,
        "dateOfBirth": dateOfBirth,
        "patronus": patronus,
        "actor": actor,
        "ancestry": ancestry,
      };
}
