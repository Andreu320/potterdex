import 'package:flutter/material.dart';
import 'package:potterdex/pages/home_screes.dart';

void main() {
  runApp(Potterdex());
}

class Potterdex extends StatelessWidget {
  const Potterdex({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
    );
  }
}
