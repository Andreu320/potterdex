import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:http/http.dart' as http;

class Hufflepuff extends StatefulWidget {
  const Hufflepuff({super.key});

  @override
  State<Hufflepuff> createState() => _HufflepuffState();
}

class _HufflepuffState extends State<Hufflepuff> {
  List hufdex = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (mounted) {
      fetchHufData();
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Hufflepuff"),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(32),
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                    'https://i.pinimg.com/originals/c5/4e/ab/c54eab71f619fc46ae47781cb7f78c47.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              children: [
                hufdex != null
                    ? Expanded(
                        child: GridView.builder(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 1,
                          childAspectRatio: 1.4,
                        ),
                        itemCount: hufdex.length,
                        itemBuilder: ((context, index) {
                          return Card(
                            color: Color.fromARGB(255, 159, 202, 18),
                            child: Stack(children: [
                              Align(
                                alignment: const Alignment(0.0, -1.0),
                                child: Text(
                                  hufdex[index]['name'],
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 35,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'HarryPotter',
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, -0.5),
                                child: Text(
                                  "Genero: " + hufdex[index]['gender'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                  alignment: const Alignment(-0.93, -0.3),
                                  child: hufdex[index]['actor'] != null
                                      ? Text(
                                          "Actor: " + hufdex[index]['actor'],
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        )
                                      : Text(
                                          "No se encontró",
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        )),
                              Align(
                                alignment: const Alignment(-0.93, -0.1),
                                child: Text(
                                  "Fecha de nacimiento: " +
                                      hufdex[index]['dateOfBirth'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, 0.1),
                                child: Text(
                                  "Patronus: " + hufdex[index]['patronus'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, 0.3),
                                child: Text(
                                  "Ancestry: " + hufdex[index]['ancestry'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(1, 1),
                                child: Container(
                                    child: CachedNetworkImage(
                                  imageUrl:
                                      'https://i.pinimg.com/originals/3d/db/2f/3ddb2f1758b2c930a3100aa3c99cd6c7.png',
                                  width: 150,
                                  height: 150,
                                )),
                              )
                            ]),
                          );
                        }),
                      ))
                    : Center(
                        child: CircularProgressIndicator(),
                      )
              ],
            )),
      ),
    );
  }

  void fetchHufData() {
    var url =
        Uri.https("hp-api.onrender.com", "/api/characters/house/hufflepuff");
    http.get(url).then((value) {
      if (value.statusCode == 200) {
        var decodedJsondata = jsonDecode(value.body);
        hufdex = decodedJsondata;
        setState(() {});
      }
    });
  }
}
