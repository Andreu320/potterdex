import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:http/http.dart' as http;

class Spells extends StatefulWidget {
  const Spells({super.key});

  @override
  State<Spells> createState() => _SpellsState();
}

class _SpellsState extends State<Spells> {
  List spellsdex = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (mounted) {
      fetchspellsData();
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Hechizos"),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(32),
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                    'https://mobimg.b-cdn.net/v3/fetch/62/62e3ce60fc426fe6f475764cd99779b9.jpeg'),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              children: [
                spellsdex != null
                    ? Expanded(
                        child: GridView.builder(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 1,
                          childAspectRatio: 1.4,
                        ),
                        itemCount: spellsdex.length,
                        itemBuilder: ((context, index) {
                          return Card(
                            color: Color.fromARGB(255, 9, 59, 66),
                            child: Stack(children: [
                              Align(
                                alignment: const Alignment(0.0, -1.0),
                                child: Text(
                                  spellsdex[index]['name'],
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 35,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'HarryPotter',
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(0.0, 0.0),
                                child: Text(
                                  spellsdex[index]['description'],
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                            ]),
                          );
                        }),
                      ))
                    : Center(
                        child: CircularProgressIndicator(),
                      )
              ],
            )),
      ),
    );
  }

  void fetchspellsData() {
    var url = Uri.https("hp-api.onrender.com", "/api/spells");
    http.get(url).then((value) {
      if (value.statusCode == 200) {
        var decodedJsondata = jsonDecode(value.body);
        spellsdex = decodedJsondata;
        setState(() {});
      }
    });
  }
}
