import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:http/http.dart' as http;

class Gryffindor extends StatefulWidget {
  const Gryffindor({super.key});

  @override
  State<Gryffindor> createState() => _GryffindorState();
}

class _GryffindorState extends State<Gryffindor> {
  List grydex = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (mounted) {
      fetchgryData();
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Gryffindor"),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(32),
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                    'https://i.pinimg.com/originals/19/e7/d5/19e7d55aeec89b200c45fd2e03e8fff2.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              children: [
                grydex != null
                    ? Expanded(
                        child: GridView.builder(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 1,
                          childAspectRatio: 1.4,
                        ),
                        itemCount: grydex.length,
                        itemBuilder: ((context, index) {
                          return Card(
                            color: Color.fromARGB(255, 89, 24, 20),
                            child: Stack(children: [
                              Align(
                                alignment: const Alignment(0.0, -1.0),
                                child: Text(
                                  grydex[index]['name'],
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 35,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'HarryPotter'),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, -0.5),
                                child: Text(
                                  "Genero: " + grydex[index]['gender'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, -0.3),
                                child: Text(
                                  "Actor: " + grydex[index]['actor'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, -0.1),
                                child: Text(
                                  "Fecha de nacimiento: " +
                                      grydex[index]['dateOfBirth'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, 0.1),
                                child: Text(
                                  "Patronus: " + grydex[index]['patronus'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, 0.3),
                                child: Text(
                                  "Ancestry: " + grydex[index]['ancestry'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Positioned(
                                  bottom: 10,
                                  right: 10,
                                  child: CachedNetworkImage(
                                    imageUrl:
                                        'https://1000marcas.net/wp-content/uploads/2021/11/Gryffindor-Logo.png',
                                    width: 200,
                                    height: 200,
                                  ))
                            ]),
                          );
                        }),
                      ))
                    : Center(
                        child: CircularProgressIndicator(),
                      )
              ],
            )),
      ),
    );
  }

  void fetchgryData() {
    var url =
        Uri.https("hp-api.onrender.com", "/api/characters/house/gryffindor");
    http.get(url).then((value) {
      if (value.statusCode == 200) {
        var decodedJsondata = jsonDecode(value.body);
        grydex = decodedJsondata;
        setState(() {});
      }
    });
  }
}
