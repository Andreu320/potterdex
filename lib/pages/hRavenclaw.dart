import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:http/http.dart' as http;

class Ravenclaw extends StatefulWidget {
  const Ravenclaw({super.key});

  @override
  State<Ravenclaw> createState() => _RavenclawState();
}

class _RavenclawState extends State<Ravenclaw> {
  List spellsdex = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (mounted) {
      fetchravData();
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ravenclaw"),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(32),
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                    'https://i.pinimg.com/originals/6d/ed/03/6ded03a78ba6b8d870c899586117245a.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              children: [
                spellsdex != null
                    ? Expanded(
                        child: GridView.builder(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 1,
                          childAspectRatio: 1.4,
                        ),
                        itemCount: spellsdex.length,
                        itemBuilder: ((context, index) {
                          return Card(
                            color: Color.fromARGB(255, 0, 105, 119),
                            child: Stack(children: [
                              Align(
                                alignment: const Alignment(0.0, -1.0),
                                child: Text(
                                  spellsdex[index]['name'],
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 35,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'HarryPotter',
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, -0.5),
                                child: Text(
                                  "Genero: " + spellsdex[index]['gender'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, -0.3),
                                child: Text(
                                  "Actor: " + spellsdex[index]['actor'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.90, -0.1),
                                child: Text(
                                  "Fecha de nacimiento: " +
                                      spellsdex[index]['dateOfBirth'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, 0.1),
                                child: Text(
                                  "Patronus: " + spellsdex[index]['patronus'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, 0.3),
                                child: Text(
                                  "Ancestry: " + spellsdex[index]['ancestry'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(1, 1),
                                child: Container(
                                    child: CachedNetworkImage(
                                  imageUrl:
                                      'https://www.pngmart.com/files/12/Ravenclaw-House-PNG-Transparent-Image.png',
                                  width: 150,
                                  height: 150,
                                )),
                              )
                            ]),
                          );
                        }),
                      ))
                    : Center(
                        child: CircularProgressIndicator(),
                      )
              ],
            )),
      ),
    );
  }

  void fetchravData() {
    var url =
        Uri.https("hp-api.onrender.com", "/api/characters/house/ravenclaw");
    http.get(url).then((value) {
      if (value.statusCode == 200) {
        var decodedJsondata = jsonDecode(value.body);
        spellsdex = decodedJsondata;
        setState(() {});
      }
    });
  }
}
