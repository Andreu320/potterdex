import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:http/http.dart' as http;

class Slytherin extends StatefulWidget {
  const Slytherin({super.key});

  @override
  State<Slytherin> createState() => _SlytherinState();
}

class _SlytherinState extends State<Slytherin> {
  List spellsdex = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (mounted) {
      fetchslyData();
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Slythetin"),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(32),
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                    'https://i.pinimg.com/originals/43/b3/81/43b3810a442811b7d2ae5c312db1de6f.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              children: [
                spellsdex != null
                    ? Expanded(
                        child: GridView.builder(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 1,
                          childAspectRatio: 1.4,
                        ),
                        itemCount: spellsdex.length,
                        itemBuilder: ((context, index) {
                          return Card(
                            color: Color.fromARGB(255, 17, 175, 96),
                            child: Stack(children: [
                              Align(
                                alignment: const Alignment(0.0, -1.0),
                                child: Text(
                                  spellsdex[index]['name'],
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 35,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'HarryPotter',
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, -0.5),
                                child: Text(
                                  "Genero: " + spellsdex[index]['gender'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, -0.3),
                                child: Text(
                                  "Actor: " + spellsdex[index]['actor'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.90, -0.1),
                                child: Text(
                                  "Fecha de nacimiento: " +
                                      spellsdex[index]['dateOfBirth'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, 0.1),
                                child: Text(
                                  "Patronus: " + spellsdex[index]['patronus'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(-0.93, 0.3),
                                child: Text(
                                  "Ancestry: " + spellsdex[index]['ancestry'],
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: const Alignment(1, 1),
                                child: Container(
                                    child: CachedNetworkImage(
                                  imageUrl:
                                      'https://logos-world.net/wp-content/uploads/2022/02/Slytherin-Symbol.png',
                                  width: 250,
                                  height: 250,
                                )),
                              )
                            ]),
                          );
                        }),
                      ))
                    : Center(
                        child: CircularProgressIndicator(),
                      )
              ],
            )),
      ),
    );
  }

  void fetchslyData() {
    var url =
        Uri.https("hp-api.onrender.com", "/api/characters/house/slytherin");
    http.get(url).then((value) {
      if (value.statusCode == 200) {
        var decodedJsondata = jsonDecode(value.body);
        spellsdex = decodedJsondata;
        setState(() {});
      }
    });
  }
}
