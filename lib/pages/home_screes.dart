import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:potterdex/pages/hHufflepuff.dart';
import 'package:potterdex/pages/hRavenclaw.dart';
import 'package:potterdex/pages/hgryffindor.dart';
import 'package:potterdex/pages/hslytherin.dart';
import 'package:potterdex/pages/spells.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("PotterDex"),
        centerTitle: true,
      ),
      body: Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.all(32),
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(
                  'https://i.pinimg.com/originals/88/73/e5/8873e5b6e68967aa9ca996cb790c9f24.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: Stack(children: [
            Align(
              alignment: const Alignment(0.0, -0.3),
              child: ElevatedButton(
                child: const Text('Gryffindor'),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Gryffindor()));
                },
              ),
            ),
            Align(
              alignment: const Alignment(0.0, -0.15),
              child: ElevatedButton(
                child: const Text("Slytherin"),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Slytherin()));
                },
              ),
            ),
            Align(
                alignment: const Alignment(0.0, -0.0015),
                child: ElevatedButton(
                  child: const Text('Hufflepuff'),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Hufflepuff()));
                  },
                )),
            Align(
                alignment: const Alignment(0.0, 0.15),
                child: ElevatedButton(
                  child: const Text('Ravenclaw'),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Ravenclaw()));
                  },
                )),
            Align(
                alignment: const Alignment(0.0, 0.30),
                child: ElevatedButton(
                  child: const Text('Hechizos'),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Spells()));
                  },
                )),
          ])),
    );
  }
}
